<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>
Arbitros Partido
</title>
</head>
<body>
<h2>
arbitros_partidos 
</h2>
<html:form action="/arbitrosPartidos">

<bean:message key="arbitroPartido.arbitro" />
 
<html:select property="arbitro">
<html:options collection="ayuda" labelProperty="desc" property="codigo" />
</html:select> 
<bean:message key="arbitroPartido.partido" />
<html:select property="partido">
<html:options collection="ayuda2" labelProperty="desc" property="codigo" />
</html:select>

<bean:message key="arbitroPartido.tipo_arbitro" />
<html:select property="tipo_arbitro">
<html:options collection="ayuda3" labelProperty="desc" property="codigo" />
</html:select>
<html:submit value="Registrar" />

</html:form>
</body>
</html>
