<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta charset="UTF-8">
<title>
Grand Slam
</title>

<!--Estilos css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/login.css">

<!--Scripts -->
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/login.js"></script>
</head>
<body id="LoginForm">  
  <div class="container col-md-3 col-md-offset-8" style="padding-top:100px;">
      <div class="card" style="max-width: 15rem;">
        <div class="card-header" ><h4>Login</h4></div>
        <div class="card-body" >
           <html:form action="/inicio">
              <div class="form-group">
                <html:errors />
              </div>
              <div class="form-group">
                <bean:message key="usuario.prompt" />
                <html:text property="user" />
              </div>
              <div class="form-group">
                <bean:message key="clave.prompt" />
                <html:password property="clave" />
              </div>
              <html:submit value="Ingresar"/>
            </html:form>
        </div>
      </div>
  </div>
  <!-- Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
