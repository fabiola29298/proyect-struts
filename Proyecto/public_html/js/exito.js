$(document).ready(function(){
  function init(){
    /*Estilos de formulario*/
    $("input[type='text']").addClass("form-control");
    $("select").addClass("form-control");
    $("input[type='submit']").addClass("btn btn-success btn-block");
  }
  init();
});
