$(document).ready(function(){
  function init(){
    /*Estilos de formulario*/
    $("input[type='text']").addClass("form-control");
    $("input[type='password']").addClass("form-control");
    $("input[type='submit']").addClass("btn btn-success btn-block");
    /*Atributos de campo texto*/
    $("input[type='text']").attr("Placeholder","Usuario");
    $("input[type='password']").attr("Placeholder","Password");
    /*Borrado de atributos despues de 5 segundos*/
    setTimeout(function(){
      console.log($('#error_user'));
      if ($('#error_user').length > 0) {
        $('#error_user').remove();
      }
    }, 2000);
    setTimeout(function(){
      if ($('#error_pass').length > 0) {
        $('#error_pass').remove();
      }
    },3000);


  }
  init();
});
