$(document).ready(function(){
  function init(){
    /*Estilos de formulario*/
    $("input[type='text']").addClass("form-control");
    $("select").addClass("form-control");
    $("input[name='fecha_inicio']").attr("type","date");
    $("input[name='fecha_final']").attr("type","date");
    $("input[type='submit']").addClass("btn btn-success btn-block");
  }
  init();
});
