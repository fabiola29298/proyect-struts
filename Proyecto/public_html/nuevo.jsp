<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>
Registro de torneo
</title>
<!--Estilos css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/torneo_nuevo.js"></script>
</head>
<body style="background-color:#F2EFE4;">
<div style="background-color:#CB8C1D;">
          <div class="container">
                <h2 style="color: white;">
                 Registro de Torneo
                </h2>
                <hr>
            </div>
          </div>

    <div class="container col-md-5 col-md-offset-8" style="padding-top:12px;">
          <div class="card" style="max-width: 45rem;">
            <div class="card-body">
               <html:form action="/nuevo">
                    <div class="form-group">
                      <label for="Nombre"><bean:message key="nombre.nom"/></label>
                      <br>
                      <html:text property="nombre" />
                    </div>  
                    <div class="form-group">
                      <label for="Fecha_Inicio"><bean:message key="fecha.ini"/></label>
                      <br>
                      <html:text property="fecha_inicio" />
                    </div> 
                    <div class="form-group">
                      <label for="Fecha_Final"><bean:message key="fecha.fin"/></label>
                      <br>
                      <html:text property="fecha_final" />
                    </div>
                    <div class="form-group">
                      <label for="Gestion"><bean:message key="edicion.edi"/></label>
                      <br>
                      <html:select property="edicion">
                              <html:options collection="combo_gestion" property="id" labelProperty="gestion" />
                      </html:select>
                    </div>
                    <div class="form-group">
                      <label for="Pais"><bean:message key="pais.pais"/></label>
                      <br>
                      <html:select property="pais">
                              <html:options collection="combo_pais" property="id" labelProperty="nombre" />
                      </html:select>
                    </div>
                   <div class="form-group">
                         <html:submit value="Registrar"/>
                  </div>
                </html:form>
            </div>
          </div>
      </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
