<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>
Registro Entrenadores
</title>
<!--Estilos css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/exito.js"></script>

</head>
<body style="background-color:#F2EFE4;">
    <div style="background-color:#CB8C1D;">
          <div class="container">
              <h2 style="color: white;">
              Registro de Entrenador
              </h2>
              <hr>
          </div>
        </div>
    <div class="container col-md-5 col-md-offset-8" style="padding-top:100px;">
      <div class="card" style="max-width: 45rem;">
        <div class="card-body">
            <html:form action="/p_entrenadores">
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                          <label for="Nombre">Nombre</label>
                          <br>
                          <html:text property="nombre" />
                        </div>             
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                          <label for="Apellido_Paterno">Apellido Paterno</label>
                          <html:text property="apaterno" />
                        </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                          <label for="Apellido_Materno">Apellido Materno</label>
                          <html:text property="amaterno" />
                        </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                          <label for="Fono">Fono</label>
                          <br>
                          <html:text property="fono" />
                        </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                            <label for="Pasaporte">Pasaporte</label>
                            <br>
                            <html:text property="pasaporte" />
                      </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                          <label for="Genero">Genero</label>
                          <br>
                          <html:select property="genero">
                                  <html:options collection="combo_genero" property="id" labelProperty="nombre" />
                          </html:select>
                        </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                         <div class="form-group">
                            <label for="Nacionalidad">Nacionalidad</label>
                            <br>
                             <html:select property="nacionalidad">
                                      <html:options collection="combo_nacionalidad" property="id" labelProperty="nombre" />
                              </html:select>
                          </div>
                   </div>
                   <div class="col-md-6">
                   </div>
               </div>
               <div class="form-group">
                     <html:submit value="Registrar"/>
              </div>
            </html:form>
        </div>
      </div>
  </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
