<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>
Participantes
</title>

<!--Estilos css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/exito.js"></script>

</head>
<body style="background-color:#F2EFE4;">
  <div style="background-color:#CB8C1D;">
    <div class="container">
        <h2 style="color: white;">
        Participantes
        </h2>
        <hr>
    </div>
  </div>
   <div class="container col-md-6 col-md-offset-8" style="padding-top:100px;">
      <div class="card" style="max-width: 80rem;">
        <div class="card-body">
          <html:form action="/participantes">
              <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                    <html:submit property="boton" value="JUGADOR" />
                 </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <html:submit property="boton" value="VER JUGADOR" />
                   </div>
                </div>
              </div>
               <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                    <html:submit property="boton" value="ARBITRO" />
                 </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <html:submit property="boton" value="VER ARBITRO" />
                   </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                   <html:submit property="boton" value="ENTRENADOR" />
                 </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <html:submit property="boton" value="VER ENTRENADOR" />
                   </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                 <div class="form-group">
                   <html:submit property="boton" value="VOLVER" />
                 </div>
                </div>
              </div>              
          </html:form>
        </div>
      </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>
</html>
