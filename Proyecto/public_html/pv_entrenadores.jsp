<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>
Entrenadores Registrados 
</title>
<!--Estilos css-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/exito.js"></script>

</head>
<body style="background-color:#F2EFE4;">
    <div style="background-color:#CB8C1D;">
          <div class="container">
              <h2 style="color: white;">
               Entrenadores Registrados
              </h2>
              <hr>
          </div>
        </div>
     <div class="container">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Fono</th>
              <th scope="col">Genero</th>
              <th scope="col">Nacionalidad</th>
            </tr>
          </thead>
          <tbody>
            <logic:iterate id="tabla" indexId="index" name="nn" property="tabla" >
                      <tr>
                        <td><bean:write name="tabla" property="nombre" /></td>
                        <td><bean:write name="tabla" property="fono" /></td>
                        <td><bean:write name="tabla" property="genero" /></td>
                        <td><bean:write name="tabla" property="nacionalidad" /></td>
                      </tr>
            </logic:iterate>
          </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>
