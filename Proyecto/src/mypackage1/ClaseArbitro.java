package mypackage1;

public class ClaseArbitro 
{
  String codigo;
  String desc;

  public ClaseArbitro()
  {
  }

  public String getCodigo()
  {
    return codigo;
  }

  public void setCodigo(String newCodigo)
  {
    codigo = newCodigo;
  }

  public String getDesc()
  {
    return desc;
  }

  public void setDesc(String newDesc)
  {
    desc = newDesc;
  }
}