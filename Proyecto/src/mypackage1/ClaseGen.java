package mypackage1;

public class ClaseGen 
{
  String id;
  String nombre;

  public ClaseGen()
  {
  }

  public String getId()
  {
    return id;
  }

  public void setId(String newId)
  {
    id = newId;
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }
}