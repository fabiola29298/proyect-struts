package mypackage1;

public class ClaseGes 
{
  String id;
  String gestion;

  public ClaseGes()
  {
  }

  public String getId()
  {
    return id;
  }

  public void setId(String newId)
  {
    id = newId;
  }

  public String getGestion()
  {
    return gestion;
  }

  public void setGestion(String newGestion)
  {
    gestion = newGestion;
  }
}