package mypackage1;

public class ClaseLArbitro 
{
  String nombre;
  String fono;
  String genero;
  String nacionalidad;

  public ClaseLArbitro()
  {
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }

  public String getFono()
  {
    return fono;
  }

  public void setFono(String newFono)
  {
    fono = newFono;
  }

  public String getGenero()
  {
    return genero;
  }

  public void setGenero(String newGenero)
  {
    genero = newGenero;
  }

  public String getNacionalidad()
  {
    return nacionalidad;
  }

  public void setNacionalidad(String newNacionalidad)
  {
    nacionalidad = newNacionalidad;
  }
}