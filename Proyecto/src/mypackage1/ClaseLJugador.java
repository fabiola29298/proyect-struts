package mypackage1;

public class ClaseLJugador 
{
  String nombre;
  String fono;
  String estatura;
  String genero;
  String peso;

  public ClaseLJugador()
  {
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }

  public String getFono()
  {
    return fono;
  }

  public void setFono(String newFono)
  {
    fono = newFono;
  }

  public String getEstatura()
  {
    return estatura;
  }

  public void setEstatura(String newEstatura)
  {
    estatura = newEstatura;
  }

  public String getGenero()
  {
    return genero;
  }

  public void setGenero(String newGenero)
  {
    genero = newGenero;
  }

  public String getPeso()
  {
    return peso;
  }

  public void setPeso(String newPeso)
  {
    peso = newPeso;
  }
}