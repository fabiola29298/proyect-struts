package mypackage1;

public class ClaseTorneo 
{
  String id;
  String nombre;
  String fechai;
  String fechaf;
  String pais;
  String gestion;

  public ClaseTorneo()
  {
  }

  public String getId()
  {
    return id;
  }

  public void setId(String newId)
  {
    id = newId;
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }

  public String getFechai()
  {
    return fechai;
  }

  public void setFechai(String newFechai)
  {
    fechai = newFechai;
  }

  public String getFechaf()
  {
    return fechaf;
  }

  public void setFechaf(String newFechaf)
  {
    fechaf = newFechaf;
  }

  public String getPais()
  {
    return pais;
  }

  public void setPais(String newPais)
  {
    pais = newPais;
  }

  public String getGestion()
  {
    return gestion;
  }

  public void setGestion(String newGestion)
  {
    gestion = newGestion;
  }
}