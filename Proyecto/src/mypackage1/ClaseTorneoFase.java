package mypackage1;

public class ClaseTorneoFase 
{
  String codigo;
  String desc;

  public ClaseTorneoFase()
  {
  }

  public String getCodigo()
  {
    return codigo;
  }

  public void setCodigo(String newCodigo)
  {
    codigo = newCodigo;
  }

  public String getDesc()
  {
    return desc;
  }

  public void setDesc(String newDesc)
  {
    desc = newDesc;
  }
}