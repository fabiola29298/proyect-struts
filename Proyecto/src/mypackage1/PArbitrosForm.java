package mypackage1;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;

public class PArbitrosForm extends ActionForm 
{
  String nombre;
  String apaterno;
  String amaterno;
  String fono;
  String pasaporte;
  String genero;
  String nacionalidad;

  /**
   * Reset all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    super.reset(mapping, request);
  }

  /**
   * Validate all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   * @return ActionErrors A list of all errors found.
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    return super.validate(mapping, request);
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }

  public String getApaterno()
  {
    return apaterno;
  }

  public void setApaterno(String newApaterno)
  {
    apaterno = newApaterno;
  }

  public String getAmaterno()
  {
    return amaterno;
  }

  public void setAmaterno(String newAmaterno)
  {
    amaterno = newAmaterno;
  }

  public String getFono()
  {
    return fono;
  }

  public void setFono(String newFono)
  {
    fono = newFono;
  }

  public String getPasaporte()
  {
    return pasaporte;
  }

  public void setPasaporte(String newPasaporte)
  {
    pasaporte = newPasaporte;
  }

  public String getGenero()
  {
    return genero;
  }

  public void setGenero(String newGenero)
  {
    genero = newGenero;
  }

  public String getNacionalidad()
  {
    return nacionalidad;
  }

  public void setNacionalidad(String newNacionalidad)
  {
    nacionalidad = newNacionalidad;
  }
}