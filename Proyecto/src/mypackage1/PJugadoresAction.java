package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*Se debe copiar en el actionform*/
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import oracle.jdbc.*;
import java.util.*;


public class PJugadoresAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
    

    PJugadoresForm x = (PJugadoresForm) form;
    int id = getIndice("persona")+1; 
    String nombre = x.getNombre();
    String apaterno = x.getApaterno();
    String amaterno = x.getAmaterno();
    String fono = x.getFono();
    String pasa = x.getPasaporte();
    String genero = x.getGenero();
    String estatura = x.getEstatura();
    String peso = x.getPeso();
    //int insertar = m.insertarPersona(id,nombre,apaterno,amaterno , fono,pasa ,genero); 
    Connection cn = null;
    ConnectDB conn =new ConnectDB(); // solo es necesario instanciar un solo objeto
    ResultSet rsConsulta = null;
     try
     {
       cn = conn.conexion;
       String cadena = "insert into \"ar111_persona\" values('"+id+"','"+nombre+"','"+apaterno+"','"+amaterno+"','"+fono+"','"+pasa+"','"+genero+"')";
       System.out.println(cadena);
       int a = conn.InsertaDatos(cadena);
       cadena = "insert into \"ar111_jugador\" values('"+(getIndice("jugador")+1)+"',"+peso+","+estatura+","+id+")";
       System.out.println(cadena);
       a = conn.InsertaDatos(cadena);
       return mapping.findForward("participantes");
     }
     catch(Exception e)
     {
        e.printStackTrace();
        return mapping.findForward("malo");
     }
     finally
     {
      conn.closeConnection();	
     }
  }
  public int getIndice(String nombre_tabla)
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    try
    {
      cn = conn.conexion;
      String cadena = "select * from \"ar111_"+nombre_tabla+"\"";
      System.out.println(cadena);
      rsConsulta = conn.getData(cadena);
      int cont = 0;
      while (rsConsulta.next())
      {
          cont++;
  	  }  
  	  return cont;
  	}
	
    catch(Exception e)
    {
      e.printStackTrace();
      return -1;
    }
    finally
    {
      conn.closeConnection();	
    }
    
  }
}
