package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*Se debe copiar en el actionform*/
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import oracle.jdbc.*;
import java.util.*;


public class ParticipantesAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
    /*Conexion a la Bdd*/
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    /*---fin---*/
    
  
    ParticipantesForm x =(ParticipantesForm) form;
    String boton = x.getBoton();
    System.out.println("****************** "+boton+" *************");
    if(boton.equals("JUGADOR"))
    {
        genero(request);
        return mapping.findForward("jugador");
    }
    else if(boton.equals("ARBITRO"))
    {
        genero(request);
        nacionalidad(request);
        return mapping.findForward("arbitro");  
    }
    else if(boton.equals("ENTRENADOR"))
    {
        genero(request);
        nacionalidad(request);
        return mapping.findForward("entrenador");  
    }
    else if(boton.equals("VER JUGADOR"))
    {
        listaJugador(request);
        return mapping.findForward("v_jugador");  
    }
    else if(boton.equals("VER ARBITRO"))
    {
        listaArbitro(request);
        return mapping.findForward("v_arbitro");  
    }
    else if(boton.equals("VER ENTRENADOR"))
    {
        listaEntrenador(request);
        return mapping.findForward("v_entrenador");  
    }
    else if(boton.equals("VOLVER"))
    {
        listaEntrenador(request);
        return mapping.findForward("volver");  
    }
    else
    {
        return mapping.findForward("inicio");  
    }
  }

  
  public void genero(HttpServletRequest request)
  {
        Connection cn = null;
        ConnectDB conn =new ConnectDB();
        ResultSet rsConsulta = null;
        try
        {
          cn = conn.conexion;
          String cadena = "select \"id\",\"nombre\"  from \"ar111_genero\"";
          rsConsulta = conn.getData(cadena);
          ArrayList items = new ArrayList();
          while (rsConsulta.next())
          {
              ClaseGen item = new ClaseGen();
              item.setId(rsConsulta.getString("id"));
              item.setNombre(rsConsulta.getString("nombre"));
              items.add(item);
              //System.out.println("Paso ..");
          }  
          request.getSession().setAttribute("combo_genero",items);
        }
	
        catch(Exception e)
        {
          e.printStackTrace();
          //return (mapping.findForward("malo"));
        }
        finally
        {
          conn.closeConnection();	
        }
  }


  
  public void nacionalidad(HttpServletRequest request)
  {
        Connection cn = null;
        ConnectDB conn =new ConnectDB();
        ResultSet rsConsulta = null;
        try
        {
          cn = conn.conexion;
          String cadena = "select \"id_nacionalidad\",\"nombre\"  from \"ar111_nacionalidad\"";
          System.out.println(cadena);
          rsConsulta = conn.getData(cadena);
          ArrayList items = new ArrayList();
          while (rsConsulta.next())
          {
              ClaseNac item = new ClaseNac();
              item.setId(rsConsulta.getString("id_nacionalidad"));
              item.setNombre(rsConsulta.getString("nombre"));
              items.add(item);
              //System.out.println("Paso ..");
          }  
          request.getSession().setAttribute("combo_nacionalidad",items);
        }
	
        catch(Exception e)
        {
          e.printStackTrace();
          //return (mapping.findForward("malo"));
        }
        finally
        {
          conn.closeConnection();	
        }
  }
  public void listaJugador(HttpServletRequest request)
  {
      Connection cn = null;
      ConnectDB conn =new ConnectDB();
      ResultSet rsConsulta = null;
      try
      {
        cn = conn.conexion;
        String cadena = "select UPPER(b.\"nombre\")||' '||UPPER(b.\"apellido_paterno\")||' '||UPPER(b.\"apellido_materno\") as \"nombre_completo\","+
                        "b.\"fono\", c.\"nombre\",a.\"peso\",a.\"estatura\""+
                        " from  \"ar111_jugador\" a, \"ar111_persona\" b, \"ar111_genero\" c"+
                        " where a.\"id_pers\"=b.\"id_persona\" and b.\"id_gen\"=c.\"id\"";
        System.out.println(cadena);
        rsConsulta = conn.getData(cadena);
        ArrayList items = new ArrayList();
        while (rsConsulta.next())
        {
            ClaseLJugador  item = new ClaseLJugador();
            //item.setCodigo(rsConsulta.getString("id"));
            //item.setDesc(rsConsulta.getString("name"));
            item.setNombre(rsConsulta.getString("nombre_completo"));
            item.setFono(rsConsulta.getString("fono"));
            item.setGenero(rsConsulta.getString("nombre"));
            item.setPeso(rsConsulta.getString("peso"));
            item.setEstatura(rsConsulta.getString("estatura"));
            items.add(item);
            //System.out.println("Paso ..");
        }
        PVJugadoresForm f = new PVJugadoresForm ();	   
        f.setTabla(items);
        request.getSession().setAttribute("nn",f);
        //return mapping.findForward("success");
      }
      catch(Exception e)
      {
        e.printStackTrace();
        //eturn (mapping.findForward("mal"));
      }
      finally
      {
        conn.closeConnection();	
      }

  }
  public void listaArbitro(HttpServletRequest request)
  {
      Connection cn = null;
      ConnectDB conn =new ConnectDB();
      ResultSet rsConsulta = null;
      try
      {
        cn = conn.conexion;
        String cadena = "select UPPER(b. \"nombre\")||' '||UPPER(b.\"apellido_paterno\")||' '||UPPER(b.\"apellido_materno\") as \"nombre_completo\","+
                        " b.\"fono\", c.\"nombre\", d.\"nombre\" as \"nacionalidad\""+ 
                        " from  \"ar111_arbitro\" a, \"ar111_persona\" b, \"ar111_genero\" c ,\"ar111_nacionalidad\" d"+
                        " where a.\"id_perso\"=b.\"id_persona\" and b.\"id_gen\"=c.\"id\" and a.\"id_nacio\"=d.\"id_nacionalidad\"";
        System.out.println(cadena);
        rsConsulta = conn.getData(cadena);
        ArrayList items = new ArrayList();
        while (rsConsulta.next())
        {
            ClaseLArbitro  item = new ClaseLArbitro();
            //item.setCodigo(rsConsulta.getString("id"));
            //item.setDesc(rsConsulta.getString("name"));
            item.setNombre(rsConsulta.getString("nombre_completo"));
            item.setFono(rsConsulta.getString("fono"));
            item.setGenero(rsConsulta.getString("nombre"));
            item.setNacionalidad(rsConsulta.getString("nacionalidad"));
            items.add(item);
            //System.out.println("Paso ..");
        }
        PVJugadoresForm f = new PVJugadoresForm ();	   
        f.setTabla(items);
        request.getSession().setAttribute("nn",f);
        //return mapping.findForward("success");
      }
      catch(Exception e)
      {
        e.printStackTrace();
        //eturn (mapping.findForward("mal"));
      }
      finally
      {
        conn.closeConnection();	
      }
  }
  public void listaEntrenador(HttpServletRequest request)
  {
      Connection cn = null;
      ConnectDB conn =new ConnectDB();
      ResultSet rsConsulta = null;
      try
      {
        cn = conn.conexion;
        String cadena = "select UPPER(b. \"nombre\")||' '||UPPER(b.\"apellido_paterno\")||' '||UPPER(b.\"apellido_materno\") as \"nombre_completo\","+
                        " b.\"fono\", c.\"nombre\", d.\"nombre\" as \"nacionalidad\""+ 
                        " from  \"ar111_entrenador\" a, \"ar111_persona\" b, \"ar111_genero\" c ,\"ar111_nacionalidad\" d"+
                        " where a.\"id_per\"=b.\"id_persona\" and b.\"id_gen\"=c.\"id\" and a.\"id_naci\"=d.\"id_nacionalidad\"";
        System.out.println(cadena);
        rsConsulta = conn.getData(cadena);
        ArrayList items = new ArrayList();
        while (rsConsulta.next())
        {
            ClaseLEntrenador  item = new ClaseLEntrenador();
            //item.setCodigo(rsConsulta.getString("id"));
            //item.setDesc(rsConsulta.getString("name"));
            item.setNombre(rsConsulta.getString("nombre_completo"));
            item.setFono(rsConsulta.getString("fono"));
            item.setGenero(rsConsulta.getString("nombre"));
            item.setNacionalidad(rsConsulta.getString("nacionalidad"));
            items.add(item);
            //System.out.println("Paso ..");
        }
        PVJugadoresForm f = new PVJugadoresForm ();	   
        f.setTabla(items);
        request.getSession().setAttribute("nn",f);
        //return mapping.findForward("success");
      }
      catch(Exception e)
      {
        e.printStackTrace();
        //eturn (mapping.findForward("mal"));
      }
      finally
      {
        conn.closeConnection();	
      }
  }
}