package mypackage1;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;

public class arbitros_partidosForm extends ActionForm 
{
  String arbitro;
  String partido;
  String tipo_arbitro;

  /**
   * Reset all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    super.reset(mapping, request);
  }

  /**
   * Validate all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   * @return ActionErrors A list of all errors found.
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    return super.validate(mapping, request);
  }

  public String getArbitro()
  {
    return arbitro;
  }

  public void setArbitro(String newArbitro)
  {
    arbitro = newArbitro;
  }

  public String getPartido()
  {
    return partido;
  }

  public void setPartido(String newPartido)
  {
    partido = newPartido;
  }

  public String getTipo_arbitro()
  {
    return tipo_arbitro;
  }

  public void setTipo_arbitro(String newTipo_arbitro)
  {
    tipo_arbitro = newTipo_arbitro;
  }
}