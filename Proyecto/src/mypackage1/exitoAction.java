package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*Se debe copiar en el actionform*/
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import oracle.jdbc.*;
import java.util.*;

public class exitoAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
      

      
      exitoForm zzz = (exitoForm) form;     
      String boton = zzz.getBoton();
      //System.out.println(boton);
      if(boton.equals("TORNEO"))
      {
          listaTorneo(request);
          tablaTorneo(request);
          return mapping.findForward("torneo");
      }
      else
      {
          
          return mapping.findForward("participantes");
      }
 }
 public void listaTorneo(HttpServletRequest request)
 {
      Connection cn = null;
      ConnectDB conn =new ConnectDB();
      ResultSet rsConsulta = null;
      try
          {
            cn = conn.conexion;
            String cadena = "select * from \"ar111_torneo\" order by 1";
            rsConsulta = conn.getData(cadena);
            ArrayList items = new ArrayList();
            while (rsConsulta.next())
            {
                  ClaseTorneo item = new ClaseTorneo();
                  item.setId(rsConsulta.getString("id_torneo"));
                  item.setNombre(rsConsulta.getString("nombre"));
                  items.add(item);
                  System.out.println("Paso ..");
            }  
            request.getSession().setAttribute("combo_torneo",items);
          }
	
          catch(Exception e)
          {
            e.printStackTrace();
          }
          finally
          {
            conn.closeConnection();	
          }
 }
 public void tablaTorneo(HttpServletRequest request)
 {
      Connection cn = null;
      ConnectDB conn =new ConnectDB();
      ResultSet rsConsulta = null;

      try
      {
        cn = conn.conexion;
        String cadena = "select a.\"id_torneo\",a.\"nombre\", TO_CHAR(a.\"fecha_ini\",'DD/MM/YYYY') as \"fecha_ini\",TO_CHAR(a.\"fecha_fin\",'DD/MM/YYYY') as \"fecha_fin\",b.\"nombre\" as \"pais\" ,c.\"gestion\" " +
                        " from \"ar111_torneo\" a, \"ar111_pais\" b, \"ar111_gestion\" c where a.\"id_pai\"=b.\"id_pais\" and a.\"id_ges\"=c.\"id_gestion\" ";
        rsConsulta = conn.getData(cadena);
        ArrayList items = new ArrayList();
        while (rsConsulta.next())
        {
              ClaseTorneo item = new ClaseTorneo();
              item.setId(rsConsulta.getString("id_torneo"));
              item.setNombre(rsConsulta.getString("nombre"));
              item.setGestion(rsConsulta.getString("gestion"));
              item.setFechai(rsConsulta.getString("fecha_ini"));
              item.setFechaf(rsConsulta.getString("fecha_fin"));
              item.setPais(rsConsulta.getString("pais"));
              items.add(item);
              System.out.println("Paso ..");
        }
        torneoForm x =new  torneoForm();         
        x.setTabla(items);
        request.getSession().setAttribute("tabla_torneo",x);
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
      finally
      {
        conn.closeConnection();	
      }
 }
}