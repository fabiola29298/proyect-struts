package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*Se debe copiar en el actionform*/
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import oracle.jdbc.*;
import java.util.*;


public class nuevoAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB(); // solo es necesario instanciar un solo objeto
    ResultSet rsConsulta = null;

    nuevoForm aux1 =(nuevoForm) form;
    int id = getIndice("torneo")+1;
    String nombre = aux1.getNombre();
    String fecha_inicio = aux1.getFecha_inicio();
    String fecha_final = aux1.getFecha_final();
    String edicion = aux1.getEdicion();
    String pais = aux1.getPais();

       try
       {
           cn = conn.conexion;
           String cadena = "insert into \"ar111_torneo\" values ('"+id+"','"+nombre+"',TO_DATE('"+fecha_inicio+"','YYYY-MM-DD'),TO_DATE('"+fecha_final+"','YYYY-MM-DD'),'"+pais+"','"+edicion+"')";
           System.out.println(cadena);
           int a = conn.InsertaDatos(cadena);
           insertarModalidades(id);
           return mapping.findForward("exito");
  	   }
         catch(Exception e)
         {
            e.printStackTrace();
            return (mapping.findForward("malo"));
         }
  	   finally
  	   {
  		  conn.closeConnection();	
  	   }
    
  }
   public int getIndice(String nombre_tabla)
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    try
    {
      cn = conn.conexion;
      String cadena = "select * from \"ar111_"+nombre_tabla+"\"";
      System.out.println(cadena);
      rsConsulta = conn.getData(cadena);
      int cont = 0;
      while (rsConsulta.next())
      {
          cont++;
  	  }  
  	  return cont;
  	}
	
    catch(Exception e)
    {
      e.printStackTrace();
      return -1;
    }
    finally
    {
      conn.closeConnection();	
    }
    
  }
  public void insertarModalidades(int id)
  {
    Connection cn = null;
    Connection cn2 = null;
    Connection cn3 = null;
    Connection cn4 = null;
    ConnectDB conn =new ConnectDB();
    ConnectDB conn2 =new ConnectDB();
    ConnectDB conn3 =new ConnectDB();
    ConnectDB conn4 =new ConnectDB();
    ResultSet rsConsulta = null;
    ResultSet rsConsulta2 = null;
     try
    {
      cn = conn.conexion;
      cn2 = conn2.conexion;
      cn3 = conn3.conexion;
      cn4 = conn4.conexion;
      String cadena = "select * from \"ar111_modalidad\" order by 1";
      rsConsulta = conn.getData(cadena);
      while (rsConsulta.next())
      {
            int pk_tm=  getIndice("torneo_modalidad")+1;
            System.out.println(pk_tm);
            String cadena2 ="insert into \"ar111_torneo_modalidad\" values ('"+pk_tm+"','"+id+"','"+rsConsulta.getString("id_modalidad")+"')";
            System.out.println(cadena2);
            int a = conn2.InsertaDatos(cadena2);
            String cadena3 = "select * from \"ar111_fase\" order by 1";
            System.out.println(cadena3);
            rsConsulta2 = conn3.getData(cadena3);
            int cont2 = getIndice("torneo_fase")+1;
            while (rsConsulta2.next())
            {
                String cadena4 ="insert into \"ar111_torneo_fase\" values ('"+cont2+"','"+rsConsulta2.getString("id_fase")+"','"+pk_tm+"')";
                int b = conn4.InsertaDatos(cadena4);
                System.out.println(cadena4);
                cont2++;
            }
            
  	  }  
  	}
	
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      conn.closeConnection();	
      conn2.closeConnection();	
      conn3.closeConnection();
      conn4.closeConnection();	
    }
  }
}