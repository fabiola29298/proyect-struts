package mypackage1;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;

public class nuevoForm extends ActionForm 
{
  String nombre;
  String fecha_inicio;
  String fecha_final;
  String edicion;
  String pais;

  /**
   * Reset all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    super.reset(mapping, request);
  }

  /**
   * Validate all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   * @return ActionErrors A list of all errors found.
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    return super.validate(mapping, request);
  }

  public String getNombre()
  {
    return nombre;
  }

  public void setNombre(String newNombre)
  {
    nombre = newNombre;
  }

  public String getFecha_inicio()
  {
    return fecha_inicio;
  }

  public void setFecha_inicio(String newFecha_inicio)
  {
    fecha_inicio = newFecha_inicio;
  }

  public String getFecha_final()
  {
    return fecha_final;
  }

  public void setFecha_final(String newFecha_final)
  {
    fecha_final = newFecha_final;
  }

  public String getEdicion()
  {
    return edicion;
  }

  public void setEdicion(String newEdicion)
  {
    edicion = newEdicion;
  }

  public String getPais()
  {
    return pais;
  }

  public void setPais(String newPais)
  {
    pais = newPais;
  }
}