package mypackage1;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class partido_rForm extends ActionForm 
{


  String fecha;
  String hora_ini;
  String hora_fin;
  String torneo_fase;
  String stadium;
  String premio_perde;
  String premio_gana;

  /**
   * Reset all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    super.reset(mapping, request);
  }

  /**
   * Validate all properties to their default values.
   * @param mapping The ActionMapping used to select this instance.
   * @param request The HTTP Request we are processing.
   * @return ActionErrors A list of all errors found.
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    return super.validate(mapping, request);
  }

  public String getFecha()
  {
    return fecha;
  }

  public void setFecha(String newFecha)
  {
    fecha = newFecha;
  }

  public String getHora_ini()
  {
    return hora_ini;
  }

  public void setHora_ini(String newHora_ini)
  {
    hora_ini = newHora_ini;
  }

  public String getHora_fin()
  {
    return hora_fin;
  }

  public void setHora_fin(String newHora_fin)
  {
    hora_fin = newHora_fin;
  }

  public String getTorneo_fase()
  {
    return torneo_fase;
  }

  public void setTorneo_fase(String newTorneo_fase)
  {
    torneo_fase = newTorneo_fase;
  }

  public String getStadium()
  {
    return stadium;
  }

  public void setStadium(String newStadium)
  {
    stadium = newStadium;
  }

  public String getPremio_perde()
  {
    return premio_perde;
  }

  public void setPremio_perde(String newPremio_perde)
  {
    premio_perde = newPremio_perde;
  }

  public String getPremio_gana()
  {
    return premio_gana;
  }

  public void setPremio_gana(String newPremio_gana)
  {
    premio_gana = newPremio_gana;
  }









}