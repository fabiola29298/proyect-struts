package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import oracle.jdbc.*;
import java.util.*;
public class torneoAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
        torneoForm aux = (torneoForm) form;
        String boton2 = aux.getBoton2();
        if(boton2.equals("NUEVO"))
        {
            listaGestion(request);
            listaPais(request);
            return mapping.findForward("nuevo");
        }
        else if(boton2.equals("VER")) 
        {
            return mapping.findForward("torneo_opciones");
        }
        {
        return mapping.findForward("malo");
        }
  }
  public void  listaGestion(HttpServletRequest request)
  {
        Connection cn = null;
        ConnectDB conn =new ConnectDB();
        ResultSet rsConsulta = null;
        try
        {
          cn = conn.conexion;
          String cadena = "select * from \"ar111_gestion\" order by 1";
          System.out.println(cadena);
          rsConsulta = conn.getData(cadena);
          ArrayList items = new ArrayList();
          while (rsConsulta.next())
          {
                ClaseGes item = new ClaseGes();
                item.setId(rsConsulta.getString("id_gestion"));
                item.setGestion(rsConsulta.getString("gestion"));
                items.add(item);
                System.out.println("Paso ..");
          }  
          request.getSession().setAttribute("combo_gestion",items);
        }
	
        catch(Exception e)
        {
          e.printStackTrace();
        }
        finally
        {
          conn.closeConnection();	
        }
  }
  public void  listaPais(HttpServletRequest request)
  {
        Connection cn = null;
        ConnectDB conn =new ConnectDB();
        ResultSet rsConsulta = null;
        try
        {
          cn = conn.conexion;
          String cadena = "select * from \"ar111_pais\"  a where a.\"anfitrion\"=1 order by 1";
          System.out.println(cadena);
          rsConsulta = conn.getData(cadena);
          ArrayList items = new ArrayList();
          while (rsConsulta.next())
          {
                ClasePais item = new ClasePais();
                item.setId(rsConsulta.getString("id_pais"));
                item.setNombre(rsConsulta.getString("nombre"));
                items.add(item);
                System.out.println("Paso ..");
          }  
          request.getSession().setAttribute("combo_pais",items);
        }
	
        catch(Exception e)
        {
          e.printStackTrace();
        }
        finally
        {
          conn.closeConnection();	
        }
  }
}