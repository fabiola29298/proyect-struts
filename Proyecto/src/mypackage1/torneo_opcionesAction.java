package mypackage1;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException; 
import java.util.*;

public class torneo_opcionesAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
    torneo_opcionesForm aux4= (torneo_opcionesForm) form;
    String boton3 = aux4.getBoton3();

    if(boton3.equals("partido"))
    {
    //agregando datos de Torneo Fase para el comboBox
        ArrayList items = new ArrayList();
        ClaseTorneoFase item = new ClaseTorneoFase();
        item.setCodigo("57");
        item.setDesc("PRIMERA RONDA"); 
        items.add(item);
     //agregando datos de stadium para el comboBox
        ArrayList items2 = new ArrayList();
        ClaseStadium item2 = new ClaseStadium();
        item2.setCodigo("1");
        item2.setDesc("MELBOURNE"); 
        items2.add(item2);

        //log de prueba
        System.out.println("Paso a partido_r.jsp ..");  
        request.getSession().setAttribute("ayuda",items); //torneo
        request.getSession().setAttribute("ayuda2",items2);// stadium
    return mapping.findForward("partido_r");
    }else if (boton3.equals("torneo_fase"))
    {
    return mapping.findForward("torneo_fase");
    }else if (boton3.equals("torneo_gestion"))
    {
    return mapping.findForward("torneo_gestion");
    }else if (boton3.equals("jugadores"))
    {
    return mapping.findForward("jugadores");
    }else if (boton3.equals("jugadores_torneo"))
    {
    return mapping.findForward("jugadores_torneo");
    }else if (boton3.equals("entrenadores"))
    {
    return mapping.findForward("entrenadores");
    }else if (boton3.equals("arbitros"))
    {
    return mapping.findForward("arbitros");
    }else if(boton3.equals("arbitros_partidos"))
    {
    //agregando datos de arbitro para el comboBox
        ArrayList items = new ArrayList();
        ClaseArbitro item = new ClaseArbitro();
        item.setCodigo("1");
        item.setDesc("Arbitro1"); 
        items.add(item);
     //agregando datos de partido para el comboBox
        ArrayList items2 = new ArrayList();
        ClasePartido item2 = new ClasePartido();
        item2.setCodigo("1");
        item2.setDesc("partido1"); 
        items2.add(item2);
    //agregando datos de tippo arbitro  para el comboBox
        ArrayList items3 = new ArrayList();
        ClaseTipoArbitro item3 = new ClaseTipoArbitro();
        item3.setCodigo("1");
        item3.setDesc("tipo_arbitro1"); 
        items3.add(item3);
        ArrayList items4 = new ArrayList();
        items4.add(getClase1());
         //log de prueba
        System.out.println("Paso a partido_r.jsp ..");  
        request.getSession().setAttribute("ayuda",getClase3()); // arbitro
        request.getSession().setAttribute("ayuda2",getClase2());// partido
        request.getSession().setAttribute("ayuda3",getClase1());// tipo arbitro
     return mapping.findForward("arbitros_partidos");
    }
    else
    return mapping.findForward("malo");
  }
 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  
  public ArrayList getClase1()
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    ArrayList items = new ArrayList();
    try
    {
      cn = conn.conexion;
      String cadena = "select * from \"ar111_tipo_arbitro\"";
      System.out.println(cadena);
      rsConsulta = conn.getData(cadena);
      int cont = 0;
      while (rsConsulta.next())
      {
        ClaseTipoArbitro item = new ClaseTipoArbitro(); 
        item.setCodigo(rsConsulta.getString("id_tipo_arbitro"));
        item.setDesc(rsConsulta.getString("nombre"));
        items.add(item);
  	  }  
  	  return items;
  	}
    catch(Exception e)
    {
    ClaseTipoArbitro item = new ClaseTipoArbitro();
        item.setCodigo(" ");
        item.setDesc(" "); 
        items.add(item);
      e.printStackTrace();
      return items;
    }
    finally
    {
      conn.closeConnection();	
    }
    
  }
  public ArrayList getClase2()
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    ArrayList items = new ArrayList();
    try
    {
      cn = conn.conexion;
      String cadena = "select * from \"ar111_partido\"";
      System.out.println(cadena);
      rsConsulta = conn.getData(cadena);
      int cont = 0;
      while (rsConsulta.next())
      {
        ClaseTipoArbitro item = new ClaseTipoArbitro(); 
        item.setCodigo(rsConsulta.getString("id_partido"));
        item.setDesc(rsConsulta.getString("idp_partido"));
        items.add(item);
  	  }  
  	  return items;
  	}
    catch(Exception e)
    {
    ClaseTipoArbitro item = new ClaseTipoArbitro();
        item.setCodigo(" ");
        item.setDesc(" "); 
        items.add(item);
      e.printStackTrace();
      return items;
    }
    finally
    {
      conn.closeConnection();	
    }
    
  }
  public ArrayList getClase3()
  {
    Connection cn = null;
    ConnectDB conn =new ConnectDB();
    ResultSet rsConsulta = null;
    ArrayList items = new ArrayList();
    try
    {
      cn = conn.conexion;
      String cadena = "select * from \"ar111_arbitro\"";
      System.out.println(cadena);
      rsConsulta = conn.getData(cadena);
      int cont = 0;
      while (rsConsulta.next())
      {
        //String id_arbitro=rsConsulta.getString("id_arbitro");
        ClaseTipoArbitro item = new ClaseTipoArbitro(); 
        item.setCodigo(rsConsulta.getString("id_arbitro"));
        item.setDesc(rsConsulta.getString("id_arbitro"));
        items.add(item);
  	  }  
  	  return items;
  	}
    catch(Exception e)
    {
    ClaseTipoArbitro item = new ClaseTipoArbitro();
        item.setCodigo(" ");
        item.setDesc(" "); 
        items.add(item);
      e.printStackTrace();
      return items;
    }
    finally
    {
      conn.closeConnection();	
    }
    
  }
  }




